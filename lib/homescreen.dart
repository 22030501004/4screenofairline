import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<dynamic> story = [
    {"images": "assets/images/airicon1.png", "username": "Flight"},
    {"images": "assets/images/hotelicon.png", "username": "Hotel"},
    {"images": "assets/images/caricon.png", "username": "Car"},
    {"images": "assets/images/restauranticon.png", "username": "Resturant"},
    {"images": "assets/images/guesticon.png", "username": "Guest"},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.home),
        ),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: List.generate(
                      story.length,
                          (index) {
                        return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Column(
                            children: [
                              Container(
                                child: Container(
                                  width: 65,
                                  height: 65,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Container(
                                      width: 65,
                                      height: 65,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.white,
                                              spreadRadius: 2),
                                        ],
                                        image: DecorationImage(
                                          image: AssetImage(
                                              '${story[index]["images"]}'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Text('${story[index]["username"]}')
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 18.0),
                child: Text(
                  "Best Offers",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 30),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0, right: 20.0),
                    child: Text(
                      "Recommended Destination",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.grey,fontSize: 16),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 58.0),
                    child: Text(
                      "View All",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.red),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: List.generate(
                      2,
                          (index) {
                        return Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Column(children: [
                            Container(
                              child: Container(
                                width: 250,
                                height: 250,
                                decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,

                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Container(
                                    width: 250,
                                    height: 250,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(63),
                                      image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/thailand.jpeg'),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  "Thailand",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 30),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Bangkok",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,color: Colors.grey),
                                )
                              ],
                            )
                          ]),
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          ),
          Row(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0, right: 20.0),
                    child: Text(
                      "Popular places",
                      style: TextStyle(
                          fontWeight: FontWeight.w700,fontSize: 30),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 58.0),
                    child: Text(
                      "View All",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.red),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: 2,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Container(
                      height: 400,
                      width: 360,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage("assets/images/paris.jpg"),
                        ),
                      ),
                    ),

                  ],
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.black,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        iconSize: 35,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'home'),
          BottomNavigationBarItem(icon: Icon(Icons.explore_outlined), label: 'location'),
          BottomNavigationBarItem(icon: Icon(Icons.calendar_month_rounded), label: 'home'),
          BottomNavigationBarItem(icon: Icon(Icons.account_circle_outlined), label: 'user'
              ''),
        ],
      ),

    );
  }
}
