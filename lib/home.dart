import 'package:flutter/material.dart'
    show BuildContext, Center, Colors, Container, Image, InkWell, MaterialPageRoute, Navigator, Scaffold, Stack, State, StatefulWidget, Text, TextStyle, Widget;
import 'package:flutter/widgets.dart';
import 'package:untitled1/internalscreen.dart';

import 'loginpage.dart';


class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
      Container(decoration: BoxDecoration(gradient: LinearGradient(
        colors: [Color(0xff281537),Color(0xff881736),],
        begin: Alignment.bottomLeft,
        end: Alignment.topRight,),),),


    Center(

    child: InkWell(
    onTap: () {
    Navigator.of(context).push(
    MaterialPageRoute(builder: (context) {
    return LoginPage();
    }),);

    }, // Image tapped
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
    Padding(
    padding: const EdgeInsets.only(left: 38.0),
    child: Image.asset(
    'assets/images/logonew.png',width: 210.0,
    ),
    ),
    Text("Infinity",style: TextStyle(fontWeight: FontWeight.bold,fontSize:50,color: Colors.white ),),
    Text("AirLines",style: TextStyle(fontWeight: FontWeight.bold,fontSize:50,color: Colors.white ),),
    ],
    ),


    ),

    ),


    ],
    )
    ,
    );
  }
}
