import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:untitled1/homescreen.dart';

class InternalScreen extends StatefulWidget {
  const InternalScreen({super.key});

  @override
  State<InternalScreen> createState() => _InternalScreenState();
}

class _InternalScreenState extends State<InternalScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/images/airline.jpg'),
          Column(
            children: [
              Expanded(
                child: Container(),
                flex: 8,
              ),
              Container(

                margin: EdgeInsets.only(top: 100,bottom: 55),
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) {
                            return HomeScreen();
                          },
                        ),
                      );
                    });
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Color(0xff701534),
                      onPrimary: Color(0xff281537),
                      padding: EdgeInsets.only(left: 40,right: 40,top: 10,bottom:10),
                      shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  )),
                  child: Text("Start Booking",style: TextStyle(
                    fontSize: 25,
                    color: Colors.white,
                  ),),
                ),
              ),

            ],
          ),
        ],
      ),
    );
  }
}
